FROM node:18-alpine

RUN apk add chromium python3 make g++

RUN npm i -g pnpm
WORKDIR /app

COPY package.json /app/
COPY pnpm-lock.yaml /app/
RUN pnpm install

COPY timetrack.js /app/
ENTRYPOINT ["node", "timetrack.js", "punch"]
