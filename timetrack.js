#!/usr/bin/env node
const createBrowser = require('browserless');
const myArgs = process.argv.slice(2);

const CREDS = {
  username: process.env.LOGIN,
  password: process.env.PASS,
};

if (CREDS.username === undefined || CREDS.password === undefined) {
  console.error(
    'Missing credentials. Please check your environment variables.'
  );
  process.exit(128);
}

if (myArgs.length > 1) {
  console.error('Too many args. ');
  process.exit(128);
}

if (myArgs[0] && myArgs[0] !== 'punch') {
  console.info('Unknown argument, use `punch` to punch in or out.');
  process.exit(128);
}
(async () => {
  const browser = createBrowser({
    headless: 'new',
    executablePath: '/usr/bin/chromium-browser',
  });
  const browserless = await browser.createContext();
  const page = await browserless.page();
  let response = {};

  await page.setViewport({
    width: 1440,
    height: 900,
  });

  await page.goto('https://login.hrworks.de/');
  await page.waitForNetworkIdle();
  await page.type('input[name=company]', 'boersenvereinsgruppe');
  await page.keyboard.press('Tab');
  await page.waitForSelector(
    '.fade.modal.show .modal-dialog button.hrw-me-secondary-btn.me-button'
  );
  await page.waitForFunction('!document.querySelector(".blockOverlay")');
  await page.click('.modal-dialog button.hrw-me-secondary-btn.me-button');
  await page.waitForFunction(
    '!document.querySelector(".modal-dialog") && !document.querySelector(".blockOverlay")'
  );
  await page.type('input[name=login]', CREDS.username);
  await page.type('input[type=password]', CREDS.password);
  await page.waitForFunction('!document.getAnimations().length');

  await page.click('.form-body button.hrw-me-primary-btn.me-button');

  await page.waitForNavigation();
  await page.waitForSelector("[id$='36'].m-widget4__number");

  const workBalance = await page.$eval(
    "[id$='36'].m-widget4__number",
    (el) => el.innerText
  );
  const currentlyPunchedIn = await page.$eval('#m_header_topbar i', (el) =>
    Array(el.classList).some((c) => /pause/.test(c))
  );

  console.info(`Dein Zeitkonto steht auf ${workBalance}.`);
  response.message = `Dein Zeitkonto steht auf ${workBalance}.`;
  console.info(
    currentlyPunchedIn
      ? '👷 Du bist gerade eingestochen.'
      : '😴 Du bist gerade nicht eingestochen.'
  );

  if (myArgs[0] === 'punch') {
    console.info('👊 Now punching...');
    if (currentlyPunchedIn) {
      await page.click("#m_header_topbar i[class*='pause']");
      await page.waitForSelector("#m_header_topbar i[class*='play']");
      console.info('✅ Das Ausstechen war erfolgreich.');
      response.title = '✅ Das Ausstechen war erfolgreich.';
    } else {
      await page.click("#m_header_topbar i[class*='play']");
      await page.waitForSelector(
        '.fade.modal.show .modal-dialog button.hrw-me-primary-btn.me-button'
      );
      await page.waitForTimeout(1000);
      await page.click('.modal-dialog button.hrw-me-primary-btn.me-button');
      await page.waitForTimeout(1000);

      await page.waitForSelector("#m_header_topbar i[class*='pause']");
      console.info('✅ Das Einstechen war erfolgreich.');
      response.title = '✅ Das Einstechen war erfolgreich.';
    }

    // send notification via Pushcut http request
    const pushcutUrl = process.env.PUSHCUT_URL;
    if (pushcutUrl) {
      const pushcutResponse = await fetch(pushcutUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          title: response.title,
          text: response.message,
          sound: currentlyPunchedIn ? 'work-complete' : 'work',
        }),
      });
      console.info('📲 Pushcut response: ', pushcutResponse.status);
    }
  }
  // You call `destroyContext` to close the browser tab.
  await browserless.destroyContext();
  await browser.close();

  process.exit(0);
})();
